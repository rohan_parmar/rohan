import {Routes, RouterModule, Router} from '@angular/router';
import { AppComponent } from './app.component';
import { UserPostListComponent } from './userpostlist/userpostlist.component';
import { UserListComponent } from './userlist/userlist.component';

const routes:Routes = [
    {path:'',redirectTo:'AppComponent', pathMatch:'full'},
    {path:'user-list',component:UserListComponent},
    {path:'user-post',component:UserPostListComponent},
];

export const routing = RouterModule.forRoot(routes);