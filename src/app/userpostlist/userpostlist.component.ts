import {Component, OnInit} from '@angular/core';
import { UserPostService } from '../service/userpostlist';
import { PagerService } from '../service/paginationservice';
@Component({
    moduleId:module.id,
    selector:'user-post-list',
    templateUrl:'./userpostlist.component.html',
    styleUrls:['./userpostlist.component.css']
})
export class UserPostListComponent implements OnInit{
    userPostList:any;
    p: number = 1;
    collection: any[];  
    private allItems: any[];
 
    // pager object
    pager: any = {};
 
    // paged items
    pagedItems: any[];
    constructor(private _userPostService:UserPostService,
                private pagerService: PagerService){}
                setPage(page: number) {
                    if (page < 1 || page > this.pager.totalPages) {
                        return;
                    }
             
                    // get pager object from service
                    this.pager = this.pagerService.getPager(this.allItems.length, page);
             
                    // get current page of items
                    this.userPostList = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
                }
    ngOnInit(){
        this._userPostService.getUserPostList()
                             .subscribe(data => {
                                 this.allItems = data
                                this.setPage(1);
                                });
    }
}