import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class UserPostService {
    url:string ="https://jsonplaceholder.typicode.com/posts";

    constructor(private _http:Http){}

    getUserPostList(){
        return this._http.get(this.url)
                         .map(data => data.json())
    }
}