import {Pipe,PipeTransform} from '@angular/core';

@Pipe({
    name:'sort'
})

export class SortUserListService implements PipeTransform{
    transform(userList:any){
        if(userList){
            userList.sort(function(a:any,b:any){
                {
                    if(a.name<b.name){
                        return -1;
                    }else if(a.name> b.name){
                        return 1;
                    }else{
                        return 0;
                    }
                    
                }
            })
        }
        return userList;
    }
} 