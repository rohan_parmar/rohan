import {Injectable} from '@angular/core';
import {Http,Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class UserListService{
    url:string = 'https://jsonplaceholder.typicode.com/users';
    constructor(private _http:Http){}
    getUserList(){
        return this._http.get(this.url)
                         .map((data:Response)=> data.json())
    }
}