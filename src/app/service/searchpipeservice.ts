import {Pipe,PipeTransform} from '@angular/core';

@Pipe({
    name:'search'
})

export class SearchPipeUserService implements PipeTransform{
    transform(array:any,searchText:any){
        if(!array){return []}
        if(!searchText){return array}

        let searchItem = searchText.toLowerCase();
        
       return array.filter((data:any) => {
           return data.name.toLowerCase().includes(searchItem);
        }) ;
        
        
    }
}