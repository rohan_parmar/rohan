import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {NgxPaginationModule} from 'ngx-pagination';
import {PagerService} from './service/paginationservice';
import {routing} from './app.router';
// Import Component 
import { AppComponent } from './app.component';
import { UserPostListComponent } from './userpostlist/userpostlist.component';
import { UserListComponent } from './userlist/userlist.component';
//Import Service
import { UserPostService } from './service/userpostlist';
import { UserListService } from './service/userlistservice';
import {SortUserListService} from './service/usersortpipeservice';
import { SearchPipeUserService } from './service/searchpipeservice';


@NgModule({
  declarations: [
    AppComponent,
    UserPostListComponent,
    UserListComponent,
    SortUserListService,
    SearchPipeUserService,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgxPaginationModule,
    routing,
  ],
  providers: [
    UserPostService,
    PagerService,
    UserListService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
