import {Component,OnInit} from '@angular/core'
import {UserListService} from '../service/userlistservice';
import {SortUserListService} from '../service/usersortpipeservice';
import { SearchPipeUserService } from '../service/searchpipeservice';
@Component({
    selector:'user-list',
    templateUrl:'./userlist.component.html',
    styleUrls:['./userlist.component.css']
})

export class UserListComponent implements OnInit{
    userList:any;
    searchText:any;
    constructor(private _userListService:UserListService){}
    ngOnInit(){
        this._userListService.getUserList()
                             .subscribe(data => this.userList = data)
    }
}